// ==UserScript==
// @name        r/abex/dio
// @namespace   abex
// @description Fixes radio on 900px wide screens
// @include     http*://*r-a-d.io/*
// @resource    newBoot.css newBoot.css
// @resource    js.js js.js
// @version     1
// @grant       GM_getResourceText
// @run-at      document-start
// ==/UserScript==
var replaceLink = [
  {
    name:"css/bootstrap",
    replace:"newBoot.css"
  }
];
var replaceJs =[];

window.onload = function() {
  
        var css = document.createElement("script");
        css.innerHTML=GM_getResourceText("js.js");
        document.head.appendChild(css);
};
unsafeWindow.setDJ = {};
var fixed=replaceLink.length + replaceJs.length;
var stop = function() {
  if(fixed!=0) {
    setTimeout(stop,10);
  }
  else {
    console.log("Stopped");
  }
  var links = document.querySelectorAll("link");
  for(var i=0;i<links.length;i++) {
    replaceLink.forEach(function(r) {
      if(links[i].href.indexOf(r.name)!=-1) {
        console.log("Removed ", links[i].href);
        links[i].href="";
        var css = document.createElement("style");
        css.innerHTML=GM_getResourceText(r.replace);
        document.head.appendChild(css);
        fixed--;
      }
    });
  }
  var links = document.querySelectorAll("script");
  for(var i=0;i<links.length;i++) {
    var e=links[i];
    replaceJs.forEach(function(r) {
      if(links[i].src.indexOf(r.name)!=-1) {
        console.log("Removed ", links[i].src);
        e.src="";        
        var css = document.createElement("script");
        css.innerHTML=GM_getResourceText(r.replace);
        document.head.appendChild(css);
        fixed--;
      }
    });
  }
}
stop();